//javascript for slideUp error messages
$(window).on('load', function() {
  $(function() {
    setTimeout(function() {
      $('.alert').slideUp(250);
    }, 4000);
  });

  element = document.getElementById('page2');
  var rect = element.getBoundingClientRect();
  console.log(rect.top, rect.right, rect.bottom, rect.left);
});

var instance = M.Tabs.init(el, options);

// Or with jQuery

$(document).ready(function(){
  $('.tabs').tabs();
});
