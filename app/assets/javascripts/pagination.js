/* global $ */
$(document).on('turbolinks:load', function() {
  $(function() {
    $('#articles th a, #articles .pagination a').on('click', function() {
      $.getScript(this.href);
      return false;
    });
  });
  $(function() {
    $('#videos th a, #videos .pagination a').on('click', function() {
      $.getScript(this.href);
      return false;
    });
  });
  $('.p2').on('click', function() {
    $('html, body').animate(
      {
        scrollTop: $('#page2').offset().top
      },
      'slow'
    );
  });

  $('#p3').on('click', function() {
    $('html, body').animate(
      {
        scrollTop: $('#subscribe-page').offset().top
      },
      'slow'
    );
  });
});
