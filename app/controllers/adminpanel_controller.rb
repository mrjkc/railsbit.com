class AdminpanelController < ApplicationController
  #before_action :admin_user, only: [:index, :new_resource]

  def index
    @article_types = [
                        ['programming', 'programming'],['technical', 'technical'],['soft_skills', 'soft_skills'],['course', 'course'],
                        ['ruby', 'ruby'], ['web_development', 'web_development'], ['linux', 'linux'], ['database', 'database'], ['devops', 'devops'],
                        ['windows', 'windows']
                      ]
  end

  # action to add article or video
  def new_resource
    admin_redirect_link = '/adminpanel'

    @article = Article.new(article_params) unless params[:article].nil?
    @video = Video.new(video_params) unless params[:video].nil?
    if @video && @video.save
      flash[:success] = 'Video Added!'
      redirect_to admin_redirect_link
    elsif @article && @article.save
      flash[:success] = 'Article Added!'
      redirect_to admin_redirect_link
    else
      if !@article.nil?
        flash[:danger] = 'Not added!' + " " + @article.errors.full_messages
      elsif !@video.nil?
        flash[:danger] = 'Not added!' + " " + @video.errors.full_messages
      end
      redirect_to admin_redirect_link
    end
  end

private
  def article_params
    params.require(:article).permit(:title, :text, :article_type)
  end

  def video_params
    params.require(:video).permit(:title, :link, :video_type)
  end

  def admin_user
    redirect_to(root_url) && flash[:danger] = 'forbidden' unless current_user && current_user.admin?
  end
end
