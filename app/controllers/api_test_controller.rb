class ApiTestController < ApplicationController
  skip_before_action :verify_authenticity_token

  def test
    puts Hash.from_xml(request.body.read)
    respond_to do |format|
      format.xml
    end
  end
end
