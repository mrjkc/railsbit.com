class ArticlesController < ApplicationController
  def show
    @article = Article.find(params[:id])
  end

  def index
    @articles = Article.all.paginate(:page => params[:page], :per_page => 9).order('created_at DESC')
  end
end
