class StaticPagesController < ApplicationController
  require 'will_paginate/array'

  def index
    @articles = Article.order(created_at: :desc).paginate(:per_page => 9, :page => params[:page])
    @list_articles = Article.all.order('created_at DESC')
    @technical_articles = Article.where("article_type = 'programming' OR article_type = 'technical' OR article_type = 'ruby' OR article_type = 'web_development'
                                        OR article_type = 'linux' OR article_type = 'database' OR article_type = 'devops' OR article_type = 'windows'").order('created_at DESC')
    @personal_development_articles = Article.where("article_type = 'soft_skills'").order('created_at DESC')
    @videos = Video.order(created_at: :desc).paginate(:per_page => 9, :page => params[:page])
  end

  def subscribe
    @subscribe = Subscribe.new(subscribe_params)
    if (!@subscribe.nil? && @subscribe.name != "" && @subscribe.email != "") && @subscribe.save
      flash[:success] = "You have subscribed successfully!"
      redirect_to '/'
    else
      flash[:danger] = "Error, Not Subscribed" + " " + @subscribe.errors.messages.to_s
      redirect_to '/'
    end
  end

  private
  def subscribe_params
    params.require(:subscribe).permit(:name, :email)
  end
end
