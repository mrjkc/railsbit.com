class VideosController < ApplicationController
  def index
    @videos = Video.all.paginate(:page => params[:page], :per_page => 9).order('created_at DESC')
  end
end
