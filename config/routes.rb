Rails.application.routes.draw do

  get 'sessions/new'
  post 'api_test' => 'api_test#test'

  root 'static_pages#index'

  resources :users
  resources :articles
  resources :videos

  get 'signup' => 'users#new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  get 'adminpanel' => 'adminpanel#index'
  post 'adminpanel' => 'adminpanel#new_resource'

  # subscribe route
  post 'subscribe' => 'static_pages#subscribe'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
