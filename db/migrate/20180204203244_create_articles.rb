class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|

      t.string  :title
      t.text    :text
      t.boolean :liked
      t.string  :author_name
      t.integer :user_id
      t.string  :image_link
      t.string  :article_type

      t.timestamps null: false
    end
  end
end
