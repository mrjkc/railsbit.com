class CreateVideos < ActiveRecord::Migration[5.1]
  def change
    create_table :videos do |t|

      t.string  :title
      t.string  :video_type
      t.string  :link
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
